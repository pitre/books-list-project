# Books list

![UI](screenshot.png "UI")

## Prerequisites

- node8
- yarn

## Install

1. Clone this repository.
2. Run `yarn`.

## Running the application

1. yarn `mongo:start` to start dockerized mongo db instance
2. Run `yarn generate` to generate 1M books (this can take around 5min due to index creation)
3. Run `yarn start` to start the application
2. Check it out under:  `http://localhost:3000`

## Solution

### Project structure

Project is divided into three packages using yarn workspaces. All packages are under `packages` folder.
To execute commands on packages from root package use: `yarn workspace [package name] run [command]`

#### `@pw/books-list`

Isomorphic application to render books list both on server and client. SSR was implemented with `Next.js`.


#### `@pw/books-generator`

Tool to generate and populate database with books.

`yarn generate [number of books] [batch size] [host] [collection]`

* Books are generated in batches and saved in the database that way.
* Also indexes are created to speed up further filter/sort operations.


#### `@pw/mongo-client`

Package used to connect and fetch data from mongo db using await/async syntax. This package is used by `@pw/books-generate`
and `@pw/books-list`.


### Approach

* The list is rendered partially and then more data is fetched when user scrolls the list. In order to avoid keeping too
many data in the memory while scrolling elements from the beginning of the list are being removed. But if user scrolls
back up then they are fetched again.
* In order to speed up search there are indexed added in mongo database. For further improvements different group
indexes could be added.
* I've added two extra books that are horror published on Halloween and Finance published on last Friday on the month.
They are published in the future 2222 and 3333 years so sorting by date descending will bring that to the top.
* Rules that decide how to render the book entry is implemented in the BookListEntry / utils. This allows us to easily change
UI behaviour if some other requirements are needed.
* I used CSS Grid to create the layout but the list is of a fixed size. If it should be available on different device
types we should consider adding responsive or adaptive code to it.
* Also some UX could be improved by adding spinners (as the list is quite big and can take time to load) and some error
handling in UI should be added (error actions are already added).
* For a production ready product would be also good to add swagger for API documentation and render documentation from the
code (using [DocumentationJS](https://github.com/documentationjs/documentation) or something similar)
* The testing in this solution includes only unit tests. That could be extended with component, integration and e2e for
a production ready product.

### Libs/tools used

* [ES8](https://www.ecma-international.org/ecma-262/8.0/)
* [NextJS](https://github.com/zeit/next.js)
* [React](https://facebook.github.io/react/)
* [semantic-ui-react](https://react.semantic-ui.com)
* [Redux](http://redux.js.org/)
* [Redux Saga](https://redux-saga.github.io/redux-saga/)
* [axios](https://github.com/axios/axios)
* [Jest](http://facebook.github.io/jest/)
* [redux-saga-test-plan](https://github.com/jfairbank/redux-saga-test-plan)
* [Express](https://expressjs.com/)
* [Babel](https://babeljs.io/)
* [Webpack](https://webpack.js.org/)
* [ESLint](http://eslint.org/)
* [MongoDB](https://www.mongodb.com/)

## License

The MIT License (MIT)

Copyright (c) 2018 Piotr Wyrobek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
