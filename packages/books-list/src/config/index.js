const config = {
    port: 3000,
    baseUrl: 'http://localhost',
    mongo: {
        host: 'mongodb://localhost:27017/books',
        collection: 'books'
    },
    list: {
        limit: 100,
        max: 700,
        preLoadAt: 30,
        postLoadAt: 70
    }
};

/**
 * Build url
 * @param {string} path Additional path to be appended
 * @return {string} Full url
 */
export const buildUrl = path => `${config.baseUrl}:${config.port}${path || ''}`;

export default config;
