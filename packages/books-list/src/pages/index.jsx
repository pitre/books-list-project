import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import Head from 'next/head';

import configureStore from '../common/store';
import * as actions from '../common/actions';
import * as selectors from '../common/selectors';
import config from '../config';

import { BooksFilters } from '../components/BooksFilters';
import { BooksSort } from '../components/BooksSort';
import { BooksList } from '../components/BooksList';

import stylesheet from './index.scss';

/**
 * Index page component
 * @class
 */
class IndexPage extends Component {
    static async getInitialProps({ store }) {
        store.dispatch(actions.fetchBooks());
        return { };
    }

    /**
     * New filter value
     * @param {Object} filter Filter
     */
    handleFilterChange = (filter) => {
        const { filterChange, fetchBooks } = this.props;
        filterChange(filter);
        fetchBooks();
    };

    /**
     * New sort value
     * @param {Object} sort Sort
     */
    handleSortChange = (sort) => {
        const { sortChange, fetchBooks } = this.props;
        sortChange(sort);
        fetchBooks();
    };

    /**
     * On list scroll handler
     * @param {number} percentage Percentage of container scroll
     */
    onListScroll = (percentage) => {
        const { fetchBooks, data } = this.props;
        const { total, start, end, preLoading, postLoading } = data;

        if (percentage <= config.list.preLoadAt && start > 0 && !preLoading) {
            fetchBooks({ dir: -1 });
        } else if (percentage >= config.list.postLoadAt && end < total && !postLoading) {
            fetchBooks({ dir: 1 });
        }
    };

    /**
     * Render method
     * @return {Element} Html element
     */
    render() {
        const { fetchBooks, data } = this.props;
        const { books, preLoading, postLoading } = data;

        return <React.Fragment>
            <Head>
                <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.12/semantic.min.css" />
                <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
            </Head>
            <div className="container">
                <div className="main-area">
                    <div className="book-container">
                        <div className="book-head" />
                        <div className="book-filter">
                            <BooksFilters onChange={this.handleFilterChange} />
                        </div>
                        <div className="book-sort">
                            <BooksSort onChange={this.handleSortChange} />
                        </div>
                        <div className="book-main">
                            <BooksList
                                preLoading={preLoading}
                                postLoading={postLoading}
                                books={books}
                                fetchBooks={fetchBooks}
                                onScroll={this.onListScroll}
                            />
                        </div>
                        <div className="book-foot" />
                    </div>
                </div>
            </div>
        </React.Fragment>;
    }
}

IndexPage.propTypes = {
    data: PropTypes.shape({
        books: PropTypes.arrayOf(PropTypes.object),
        start: PropTypes.number,
        end: PropTypes.number,
        preLoading: PropTypes.bool,
        postLoading: PropTypes.bool
    }).isRequired
};

const mapStateToProps = state => ({
    data: selectors.selectData(state)
});

const mapDispatchToProps = (dispatch) => {
    return {
        filterChange: bindActionCreators(actions.filterChange, dispatch),
        sortChange: bindActionCreators(actions.sortChange, dispatch),
        fetchBooks: bindActionCreators(actions.fetchBooks, dispatch)
    }
};

const IndexPageHOC = withRedux(configureStore, mapStateToProps, mapDispatchToProps)(
    withReduxSaga(IndexPage)
);

export default IndexPageHOC;
