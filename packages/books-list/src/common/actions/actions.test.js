import * as actions from './';
import * as constants from '../constants';

describe('Actions', () => {
    describe('fetchBooks', () => {
        const action = actions.fetchBooks('data');

        test('produces correct type', () => {
            expect(action).toHaveProperty('type', constants.BOOKS_FETCH_REQ);
        });
        test('produces correct type', () => {
            expect(action).toHaveProperty('data', 'data');
        });
    });
    describe('fetchBooksSuccess', () => {
        const action = actions.fetchBooksSuccess('data');

        test('produces correct type', () => {
            expect(action).toHaveProperty('type', constants.BOOKS_FETCH_SUCCESS);
        });
        test('produces correct type', () => {
            expect(action).toHaveProperty('data', 'data');
        });
    });
    describe('fetchBooksError', () => {
        const action = actions.fetchBooksError('data');

        test('produces correct type', () => {
            expect(action).toHaveProperty('type', constants.BOOKS_FETCH_ERROR);
        });
        test('produces correct type', () => {
            expect(action).toHaveProperty('data', 'data');
        });
    });
    describe('filterChange', () => {
        const action = actions.filterChange('data');

        test('produces correct type', () => {
            expect(action).toHaveProperty('type', constants.FILTER_CHANGED);
        });
        test('produces correct type', () => {
            expect(action).toHaveProperty('data', 'data');
        });
    });
    describe('sortChange', () => {
        const action = actions.sortChange('data');

        test('produces correct type', () => {
            expect(action).toHaveProperty('type', constants.SORT_CHANGED);
        });
        test('produces correct type', () => {
            expect(action).toHaveProperty('data', 'data');
        });
    });
});
