import * as constants from '../constants';

export const fetchBooks = (data = { reset: true, dir: 1 }) => ({ type: constants.BOOKS_FETCH_REQ, data });
export const fetchBooksSuccess = data => ({ type: constants.BOOKS_FETCH_SUCCESS, data });
export const fetchBooksError = data => ({ type: constants.BOOKS_FETCH_ERROR, data });

export const filterChange = (data) => ({ type: constants.FILTER_CHANGED, data });
export const sortChange = (data) => ({ type: constants.SORT_CHANGED, data });
