import * as selectors from './';

describe('Selectors', () => {
    test('selectSort', () => {
        const state = {
            sort: 123
        };

        expect(selectors.selectSort(state)).toEqual(state.sort);
    });
    test('selectFilter', () => {
        const state = {
            filter: 123
        };

        expect(selectors.selectFilter(state)).toEqual(state.filter);
    });
    test('selectData', () => {
        const state = {
            data: 123
        };

        expect(selectors.selectData(state)).toEqual(state.data);
    });
});
