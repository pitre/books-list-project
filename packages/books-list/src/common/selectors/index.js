export const selectFilter = state => state.filter;
export const selectSort = state => state.sort;
export const selectData = state => state.data;
