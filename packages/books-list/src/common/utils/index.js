import * as constants from '../constants';

/**
 * Check if date is Halloween
 * @param {Date} date Date
 * @return {boolean} true or false
 */
export const isHalloween = date => (date.getDate() === 31 && date.getMonth() === 9);

/**
 * Check if date is last Friday of the month
 * @param {Date} date Date
 * @return {boolean} true or false
 */
export const isLastFriday = (date) => {
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const newYear = year + (month === 12 ? 1 : 0);
    const firstDay = new Date(Date.UTC(newYear, (month + 1) % 12, 1));
    const toFriday = (7 - (5 - firstDay.getDay())) % 7;
    const d = new Date(Date.UTC(newYear, (month + 1) % 12, 1 - toFriday));

    return (day === d.getDate()) &&
        (month === d.getMonth()) &&
        (year === d.getFullYear());
};

/**
 * Check if book is horror and was published on Halloween
 * @param {Book} book Book object
 * @return {boolean} true or false
 */
export const isHorrorOnHalloween = (book) =>
    (book.genre.id === constants.GENRES.horror.id) && isHalloween(new Date(book.publishDate));

/**
 * Check if book is finance and was published on last Friday of the month
 * @param {Book} book Book object
 * @return {boolean} true or false
 */
export const isFinanceOnLastFriday = (book) =>
    (book.genre.id === constants.GENRES.finance.id) && isLastFriday(new Date(book.publishDate));
