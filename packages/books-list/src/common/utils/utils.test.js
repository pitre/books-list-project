import * as utils from './';
import * as constants from '../constants';

describe('Utils', () => {
    const halloween = new Date(Date.UTC(2018, 9, 31));
    const lastFriday = new Date(Date.UTC(2018, 3, 27));
    const other = new Date(Date.UTC(2018, 3, 20));

    describe('isHalloween', () => {
        test('yes it is', () => {
            expect(utils.isHalloween(halloween)).toBeTruthy();
        });
        test('not this time', () => {
            expect(utils.isHalloween(other)).toBeFalsy();
        });
    });
    describe('isLastFriday', () => {
        test('yes it is', () => {
            expect(utils.isLastFriday(lastFriday)).toBeTruthy();
        });
        test('not this time', () => {
            expect(utils.isLastFriday(other)).toBeFalsy();
        });
    });
    describe('isHorrorOnHalloween', () => {
        test('wow super scary', () => {
            expect(utils.isHorrorOnHalloween({
                genre: constants.GENRES.horror,
                publishDate: halloween
            })).toBeTruthy();
        });
        test('phew just normal horror', () => {
            expect(utils.isHorrorOnHalloween({
                genre: constants.GENRES.horror,
                publishDate: other
            })).toBeFalsy();
        });
        test('ehh that not a horror', () => {
            expect(utils.isHorrorOnHalloween({
                genre: constants.GENRES.adventure,
                publishDate: halloween
            })).toBeFalsy();
        });
    });
    describe('isFinanceOnLastFriday', () => {
        test('last Friday finance', () => {
            expect(utils.isFinanceOnLastFriday({
                genre: constants.GENRES.finance,
                publishDate: lastFriday
            })).toBeTruthy();
        });
        test('just finance', () => {
            expect(utils.isFinanceOnLastFriday({
                genre: constants.GENRES.finance,
                publishDate: other
            })).toBeFalsy();
        });
        test('ehh that not a horror', () => {
            expect(utils.isFinanceOnLastFriday({
                genre: constants.GENRES.adventure,
                publishDate: lastFriday
            })).toBeFalsy();
        });
    });
});
