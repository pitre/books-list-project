import * as actions from '../actions';
import filter from './filter';

describe('Reducer filter', () => {
    test('has default state', () => {
        expect(filter()).not.toBeUndefined();
    });
    test('passes the state on unknown action', () => {
        const someState = '123';

        expect(filter(someState)).toEqual(someState);
    });
    test('updates the state', () => {
        const newFilter = {
            genre: 1,
            gender: 'female'
        };

        expect(filter(null, actions.filterChange(newFilter)))
            .toEqual(newFilter);
    });
});
