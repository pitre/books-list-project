import * as actions from '../actions';
import sort from './sort';

describe('Reducer sort', () => {
    test('has default state', () => {
        expect(sort()).not.toBeUndefined();
    });
    test('passes the state on unknown action', () => {
        const someState = '123';

        expect(sort(someState)).toEqual(someState);
    });
    test('updates the state', () => {
        const newSort = {
            sort: 'x',
            order: 'y'
        };

        expect(sort(null, actions.sortChange(newSort)))
            .toEqual(newSort);
    });
});
