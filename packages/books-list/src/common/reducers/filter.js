import * as constants from '../constants';

/**
 * Default filter state
 * @type {{genre: number, gender: number}}
 */
const defaultState = {
    'genre.id': -1,
    'author.gender': -1
};

/**
 * Filter reducer
 * @param {Object} state State
 * @param {Object} action Action
 * @return {{genre: number, gender: number}} New state
 */
const filter = (state = defaultState, action = null) => {
    const actions = {
        [constants.FILTER_CHANGED]: () => action.data
    };

    return (action && actions[action.type]) ? actions[action.type]() : state;
};

export default filter;
