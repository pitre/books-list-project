import * as actions from '../actions';
import data from './data';

describe('Reducer data', () => {
    test('has default state', () => {
        expect(data()).not.toBeUndefined();
    });
    test('passes the state on unknown action', () => {
        const someState = '123';

        expect(data(someState)).toEqual(someState);
    });
    test('updates preLading', () => {
        const state = {
            preLoading: false
        };

        expect(data(state, actions.fetchBooks({ dir: -1 })).preLoading)
            .toEqual(true);
    });
    test('updates postLading', () => {
        const state = {
            postLoading: false
        };

        expect(data(state, actions.fetchBooks({ dir: 1 })).postLoading)
            .toEqual(true);
    });
    test('resets the state', () => {
        const state = {
            start: 0,
            end: 0,
            preLoading: true,
            postLoading: false
        };
        const res = data(state, actions.fetchBooks({ reset: true }));

        expect(res.start).toEqual(0);
        expect(res.preLoading).toEqual(false);
        expect(res.postLoading).toEqual(true);
    });
    test('handle error', () => {
        const state = {
            preLoading: true,
            postLoading: true
        };
        const res = data(state, actions.fetchBooksError());

        expect(res.preLoading).toEqual(false);
        expect(res.postLoading).toEqual(false);
    });
    test('handle books fetch - inital', () => {
        const state = {
            books: [],
            start: 0,
            end: 0,
            preLoading: false,
            postLoading: true
        };
        const res = data(state, actions.fetchBooksSuccess({ reset: true, dir: 1, docs: [1,2] }));

        expect(res.preLoading).toEqual(false);
        expect(res.postLoading).toEqual(false);
        expect(res.start).toEqual(0);
        expect(res.end).toEqual(2);
        expect(res.books).toEqual([1,2]);
    });
    test('handle books fetch - prepend', () => {
        const state = {
            books: [4,5,6],
            start: 4,
            end: 6,
            preLoading: true,
            postLoading: false
        };
        const res = data(state, actions.fetchBooksSuccess({ dir: -1, docs: [0,1,2,3] }));

        expect(res.preLoading).toEqual(false);
        expect(res.postLoading).toEqual(false);
        expect(res.start).toEqual(0);
        expect(res.end).toEqual(6);
        expect(res.books).toEqual([0,1,2,3,4,5,6]);
    });
    test('handle books fetch - append', () => {
        const state = {
            books: [4,5,6],
            start: 4,
            end: 6,
            preLoading: false,
            postLoading: true
        };
        const res = data(state, actions.fetchBooksSuccess({ dir: 1, docs: [7,8,9] }));

        expect(res.preLoading).toEqual(false);
        expect(res.postLoading).toEqual(false);
        expect(res.start).toEqual(4);
        expect(res.end).toEqual(9);
        expect(res.books).toEqual([4,5,6,7,8,9]);
    });
    test('handle books fetch - reset', () => {
        const state = {
            books: [4,5,6],
            start: 4,
            end: 6,
            preLoading: false,
            postLoading: true
        };
        const res = data(state, actions.fetchBooksSuccess({ reset: true, docs: [7,8,9] }));

        expect(res.preLoading).toEqual(false);
        expect(res.postLoading).toEqual(false);
        expect(res.start).toEqual(0);
        expect(res.end).toEqual(3);
        expect(res.books).toEqual([7,8,9]);
    });
});
