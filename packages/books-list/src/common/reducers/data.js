import * as constants from '../constants';
import config from '../../config';

/**
 * Default data state
 * @type {{books: Array, start: number, end: number}}
 */
const defaultState = {
    books: [],
    start: 0,
    end: 0,
    total: 0,
    preLoading: false,
    postLoading: true
};

/**
 * Data reducer
 * @param {Object} state State
 * @param {Object} action Action
 * @return {Object} New state
 */
const data = (state = defaultState, action = null) => {
    const actions = {
        [constants.BOOKS_FETCH_REQ]: () => {
            const { dir, reset = false } = action.data;

            if (reset) {
                return { ...state, preLoading: false, postLoading: true };
            }
            return (dir < 0 ? { ...state, preLoading: true } : { ...state, postLoading: true });
        },
        [constants.BOOKS_FETCH_ERROR]: () =>
            ({ ...state, preLoading: false, postLoading: false }),
        [constants.BOOKS_FETCH_SUCCESS]: () => {
            const { dir, docs, total, reset } = action.data;
            let { books, start, end, preLoading, postLoading } = state;

            if (reset) {
                start = 0;
                end = 0;
                books = [];
            }

            if (dir > 0 || reset) {
                end = end + docs.length;
                books = [...books, ...docs];
                if (end - start > config.list.max) {
                    start = end - config.list.max;
                    books = books.slice(books.length - config.list.max);
                }
                postLoading = false;
            } else {
                start = start - docs.length;
                books = [...docs, ...books];
                if (end - start > config.list.max) {
                    end = start + config.list.max;
                    books = books.slice(0, config.list.max);
                }
                preLoading = false;
            }
            return ({ ...state, books, start, end, preLoading, postLoading, total });
        }
    };

    return (action && actions[action.type]) ? actions[action.type]() : state;
};

export default data;
