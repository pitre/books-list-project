import * as constants from '../constants';

/**
 * Default sort state
 * @type {{sort: string, order: string}}
 */
const defaultState = {
    sort: 'publishDate',
    order: 'desc'
};

/**
 * Sort reducer
 * @param {Object} state State
 * @param {Object} action Action
 * @return {{sort: string, order: string}}
 */
const sort = (state = defaultState, action = null) => {
    const actions = {
        [constants.SORT_CHANGED]: () => action.data
    };

    return (action && actions[action.type]) ? actions[action.type]() : state;
};

export default sort;
