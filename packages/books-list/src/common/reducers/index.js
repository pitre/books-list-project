import { combineReducers } from 'redux';
import filter from './filter';
import sort from './sort';
import data from './data';

export default combineReducers({
    filter,
    sort,
    data
});
