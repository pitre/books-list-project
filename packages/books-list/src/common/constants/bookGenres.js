const bookGenres = Object.freeze({
    'action-and-adventure': {
        id: 0,
        name: 'Action and Adventure'
    },
    adventure: {
        id: 1,
        name: 'Adventure'
    },
    anthology: {
        id: 2,
        name: 'Anthology'
    },
    art: {
        id: 3,
        name: 'Art'
    },
    autobiographies: {
        id: 4,
        name: 'Autobiographies'
    },
    biographies: {
        id: 5,
        name: 'Biographies'
    },
    'children\'s': {
        id: 6,
        name: 'Children\'s'
    },
    comic: {
        id: 7,
        name: 'Comic'
    },
    comics: {
        id: 8,
        name: 'Comics'
    },
    cookbooks: {
        id: 9,
        name: 'Cookbooks'
    },
    crime: {
        id: 10,
        name: 'Crime'
    },
    diaries: {
        id: 11,
        name: 'Diaries'
    },
    dictionaries: {
        id: 12,
        name: 'Dictionaries'
    },
    drama: {
        id: 13,
        name: 'Drama'
    },
    encyclopedias: {
        id: 14,
        name: 'Encyclopedias'
    },
    epistolary: {
        id: 15,
        name: 'Epistolary'
    },
    erotic: {
        id: 16,
        name: 'Erotic'
    },
    fantasy: {
        id: 17,
        name: 'Fantasy'
    },
    fiction: {
        id: 18,
        name: 'Fiction'
    },
    finance: {
        id: 19,
        name: 'Finance'
    },
    guide: {
        id: 20,
        name: 'Guide'
    },
    health: {
        id: 21,
        name: 'Health'
    },
    historical: {
        id: 22,
        name: 'Historical'
    },
    history: {
        id: 23,
        name: 'History'
    },
    horror: {
        id: 24,
        name: 'Horror'
    },
    journals: {
        id: 25,
        name: 'Journals'
    },
    magic: {
        id: 26,
        name: 'Magic'
    },
    math: {
        id: 27,
        name: 'Math'
    },
    mystery: {
        id: 28,
        name: 'Mystery'
    },
    paranoid: {
        id: 29,
        name: 'Paranoid'
    },
    philosophical: {
        id: 30,
        name: 'Philosophical'
    },
    poetry: {
        id: 31,
        name: 'Poetry'
    },
    political: {
        id: 32,
        name: 'Political'
    },
    'prayer-books': {
        id: 33,
        name: 'Prayer books'
    },
    'religion-spirituality-and-new-age': {
        id: 34,
        name: 'Religion, Spirituality & New Age'
    },
    romance: {
        id: 35,
        name: 'Romance'
    },
    saga: {
        id: 36,
        name: 'Saga'
    },
    satire: {
        id: 37,
        name: 'Satire'
    },
    science: {
        id: 38,
        name: 'Science'
    },
    'science-fiction': {
        id: 39,
        name: 'Science fiction'
    },
    'self-help': {
        id: 40,
        name: 'Self help'
    },
    series: {
        id: 41,
        name: 'Series'
    },
    speculative: {
        id: 42,
        name: 'Speculative'
    },
    superhero: {
        id: 43,
        name: 'Superhero'
    },
    thriller: {
        id: 44,
        name: 'Thriller'
    },
    travel: {
        id: 45,
        name: 'Travel'
    },
    trilogy: {
        id: 46,
        name: 'Trilogy'
    },
    urban: {
        id: 47,
        name: 'Urban'
    },
    western: {
        id: 48,
        name: 'Western'
    }
});

export default bookGenres;
