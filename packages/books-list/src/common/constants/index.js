export const BOOKS_FETCH_REQ = 'BOOKS_FETCH_REQ';
export const BOOKS_FETCH_SUCCESS = 'BOOKS_FETCH_SUCCESS';
export const BOOKS_FETCH_ERROR = 'BOOKS_FETCH_ERROR';

export const FILTER_CHANGED = 'FILTER_CHANGED';
export const SORT_CHANGED = 'SORT_CHANGED';

export { default as GENRES } from './bookGenres';
