import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { expectSaga } from 'redux-saga-test-plan';
import * as actions from '../actions';
import rootReducer from '../reducers';
import rootSaga from './';
import { buildUrl } from '../../config';

describe('Saga', () => {
    let mock = null;
    const mockData = {
        docs: [],
        total: 123,
        dir: 1,
        reset: true
    };

    beforeEach(() => {
        mock = new MockAdapter(axios);

        mock.onGet(
            buildUrl('/api/search?&sort=publishDate&order=desc&limit=100&skip=0')
        ).reply(200, mockData);
    });
    test('puts fetch success action', () => {
        return expectSaga(rootSaga)
            .withReducer(rootReducer)
            .put(actions.fetchBooksSuccess(mockData))
            .dispatch(actions.fetchBooks())
            .run();
    });
});
