import {
    put,
    takeLatest,
    select
} from 'redux-saga/effects';
import axios from 'axios';

import { buildUrl } from '../../config';
import * as constants from '../constants';
import * as actions from '../actions';
import * as selectors from '../selectors';
import config from '../../config';

/**
 * Build search query
 * @param {Object} filter Filter object
 * @param {Object} sort Sort object
 * @param {number} skip Skip first elements
 * @param {number} limit Limit results set
 * @return {string} query string
 */
const buildQuery = (filter, sort, skip, limit) => {
    const query = [
        Object.entries(filter).reduce((acc, [key, value]) => {
            if (value !== -1) {
                return `${acc}&filter=${key}=${value}`;
            }
            return acc;
        }, ''),
        ...Object.entries(sort).map(([key, value]) => `${key}=${value}`),
        `limit=${limit}`,
        `skip=${skip}`
    ];

    return query.join('&');
};

/**
 * Retrieve date from server
 * @return {Promise<Object | null>} Promise that resolves to response data
 */
function *getData(dir, reset) {
    try {
        const filter = yield select(selectors.selectFilter);
        const sort = yield select(selectors.selectSort);
        const { start, end } = yield select(selectors.selectData);
        let skip = 0;

        if (!reset) {
            skip = (dir < 0 ? start - config.list.limit : end);
        }
        const query = buildQuery(filter, sort, skip, config.list.limit);
        const response = yield axios.get(buildUrl(`/api/search?${query}`));

        return response.data;
    } catch (e) {
        console.log(e);
    }
    return null;
}

/**
 * Fetch books saga
 * @param {Object} data Action data
 */
function *fetchBooksSaga({ data }) {
    const { dir, reset } = data;

    try {
        const result = yield getData(dir, reset);

        if (result) {
            const { docs, total } = result;

            yield put(actions.fetchBooksSuccess({ docs, total, dir, reset }));
        } else {
            yield put(actions.fetchBooksError('Unknown error'));
        }
    } catch (e) {
        yield put(actions.fetchBooksError(e));
    }
}

/**
 * Root saga
 */
function *rootSaga() {
    yield takeLatest(constants.BOOKS_FETCH_REQ, fetchBooksSaga);
}

export default rootSaga;
