import * as api from './api';

describe('API helpers', () => {
    test('tryParse to parse int', () => {
        expect(api.tryParse('123')).toEqual(123);
    });
    test('tryParse not to parse string', () => {
        expect(api.tryParse('abc')).toEqual('abc');
    });
    test('build filters undefined', () => {
        expect(api.buildFilers()).toEqual({});
    });
    test('build filters empty array', () => {
        expect(api.buildFilers([])).toEqual({});
    });
    test('build filters values', () => {
        expect(api.buildFilers('test=1')).toEqual({
            test: 1
        });
    });
    test('build filters multiple values', () => {
        expect(api.buildFilers(['test=1', 'test2=2'])).toEqual({
            test: 1,
            test2: 2
        });
    });
    test('build filters same values', () => {
        expect(api.buildFilers(['test=1', 'test=2'])).toEqual({
            test: 2
        });
    });
    test('build sort empty', () => {
        expect(api.buildSort('test')).toEqual({
            test: 1
        });
    });
    test('build sort asc', () => {
        expect(api.buildSort('test', 'asc')).toEqual({
            test: 1
        });
    });
    test('build sort desc', () => {
        expect(api.buildSort('test', 'desc')).toEqual({
            test: -1
        });
    });
});
