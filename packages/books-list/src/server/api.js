import express from 'express';
import { parse } from 'url';
import { MongoClient } from '@pw/mongo-client';

import config from '../config';

const router = express.Router(); // eslint-disable-line

/**
 * Try parse int and return same value if fails
 * @param {string} value String value
 * @return {number | string} Parsed or original value
 */
export const tryParse = (value) => {
    const parsed = parseInt(value, 10);

    return isNaN(parsed) ? value : parsed;
};

/**
 * Build filters object
 * @param {Array<string>|string} filter Filter or array of filters
 * @return {Object} Filter object
 */
export const buildFilers = (filter) => {
    if (!filter) {
        return {};
    }
    if (!Array.isArray(filter)) {
        filter = [filter];
    }
    return filter.reduce((acc, f) => {
        const fsplit = f.split('=');

        return {
            ...acc,
            [fsplit[0]]: tryParse(fsplit[1])
        };
    }, {});
};

/**
 * Build sort object
 * @param {string} sort Sort name
 * @param {string} order Sort order (asc | desc)
 * @return {Object} Sort object
 */
export const buildSort = (sort, order) => ({ [sort]: order === 'desc' ? -1 : 1 });

/**
 * Search GET route
 */
router.get('/search', async (req, res) => {
    const parsed = parse(req.url, true);

    const mongo = new MongoClient(config.mongo);

    try {
        await mongo.connect();

        const {
            filter,
            sort = 'publishDate',
            order = 'desc',
            skip = '0',
            limit = '10'
        } = parsed.query;

        const data = await mongo.getDocuments({
            skip: parseInt(skip, 10),
            limit: parseInt(limit, 10),
            sort: buildSort(sort, order),
            filters: buildFilers(filter)
        });

        res.status(200).json(data);
    } catch (e) {
        res.status(500).json({ e });
    } finally {
        await mongo.close();
    }
});

export default router;
