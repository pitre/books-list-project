import React from 'react';
import renderer from 'react-test-renderer';
import { BookListEntry } from './BookListEntry';

describe('BooksList', () => {
    const book = {
        _id: '123',
        title: 'title',
        publishDate: '12/21/1985',
        author: {
            name: 'test',
            gender: 'male'
        },
        genre: {
            id: 1,
            name: 'Genre'
        }
    };

    test('Renders correctly', () => {
        const jsx = (<BookListEntry book={book} />);
        const tree = renderer
            .create(jsx)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
