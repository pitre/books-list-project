import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Segment,
    Icon,
} from 'semantic-ui-react'
import * as utils from '../../common/utils';

import stylesheet from './bookListEntry.scss';

/**
 * Books list entry component
 * @constructor
 */
class BookListEntry extends Component {
    /**
     * Format data
     * @param {Date} publishDate Date object
     * @return {string} Date in string
     */
    formatDate = (publishDate) => {
        const d = new Date(publishDate);
        return `${d.getDate()}/${(d.getMonth() + 1)}/${d.getFullYear()}`;
    };

    /**
     * Render method
     * @return {HTMLElement} Html element
     */
    render() {
        const { book } = this.props;

        return (
            <Segment vertical className='book-list-entry'>
                <style dangerouslySetInnerHTML={{__html: stylesheet}}/>
                <div className='title'>
                    {book.title} - {book.author.name}
                    <Icon name={book.author.gender} />
                </div>
                <div className='subtitle'>
                    {book.genre.name}, published on {this.formatDate(book.publishDate)}
                    {utils.isHorrorOnHalloween(book) && <span className='special'>- Halloween</span>}
                    {utils.isFinanceOnLastFriday(book) &&
                    <span className='special'>- Last Friday of the month</span>}
                </div>
            </Segment>
        );
    }
}

BookListEntry.propTypes = {
    book: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        author: PropTypes.shape({
            name: PropTypes.string.isRequired,
            gender: PropTypes.string.isRequired
        }).isRequired,
        genre: PropTypes.shape({
            name: PropTypes.string.isRequired
        }).isRequired,
        publishDate: PropTypes.string.isRequired,
    }),
};

export { BookListEntry };
