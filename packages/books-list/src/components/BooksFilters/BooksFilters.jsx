import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Dropdown
} from 'semantic-ui-react';

import * as constants from '../../common/constants';
import stylesheet from './booksFilters.scss';

const genders = [
    { key: 0, text: 'Any', value: -1 },
    { key: 1, text: 'Female', value: 'female', icon: 'female' },
    { key: 2, text: 'Male', value: 'male', icon: 'male' }
];

const genres = [
    { key: -1, text: 'Any', value: -1 },
    ...Object.values(constants.GENRES).reduce((acc, genre) => ([
        ...acc,
        { key: genre.id, text: genre.name, value: genre.id }
    ]), [])
];

/**
 * Books Filter component
 * @constructor
 */
class BooksFilters extends Component {
    state = {
        'genre.id': genres[0].value,
        'author.gender': genders[0].value
    };

    /**
     * Handle Filter change
     * @param {Object} e Event
     * @param {string} name Element name
     * @param {string} value Element value
     * @returns {void}
     */
    handleChange = (e, { name, value }) => {
        const { onChange = f => f } = this.props;

        this.setState(prevState => {
            const change = { [name]: value };

            onChange({
                ...prevState,
                ...change
            });
            return change;
        });
    };

    /**
     * Render method
     * @return {HTMLElement} Html element
     */
    render() {
        const genre = this.state['genre.id'];
        const gender = this.state['author.gender'];

        return (
            <div>
                <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
                <div className="filter-container">
                    <div className="filer-option filer-border">
                        <h1>Book genre:</h1>
                        <Dropdown
                            name='genre.id'
                            onChange={this.handleChange}
                            placeholder='Book genre'
                            fluid
                            search
                            defaultValue={genre}
                            options={genres}
                        />
                    </div>
                    <div className="filer-option">
                        <h1>Author gender:</h1>
                        <Dropdown
                            name='author.gender'
                            onChange={this.handleChange}
                            placeholder='Author gender'
                            fluid
                            search
                            defaultValue={gender}
                            options={genders}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

BooksFilters.propTypes = {
    onChange: PropTypes.func
};

BooksFilters.defaultProps = {
    onChange: f => f
};

export { BooksFilters };
