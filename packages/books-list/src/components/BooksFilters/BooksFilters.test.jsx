import React from 'react';
import renderer from 'react-test-renderer';
import { BooksFilters } from './BooksFilters';

describe('BooksFilters', () => {
    test('Renders correctly', () => {
        const jsx = (<BooksFilters />);
        const tree = renderer
            .create(jsx)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
