import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Icon
} from 'semantic-ui-react';

/**
 * Books sort component
 * @constructor
 */
class BooksSort extends Component {
    state = {
        sort: 'publishDate',
        order: 'desc'
    };

    /**
     * Handle Filter change
     * @param {Object} e Event
     * @param {string} name Element name
     * @param {string} value Element value
     * @returns {void}
     */
    handleChange = (e, { name, type }) => {
        const { onChange = f => f } = this.props;

        this.setState(prevState => {
            const change = { [type]: name };

            onChange({
                ...prevState,
                ...change,
            });
            return change;
        });
    };

    /**
     * Render method
     * @return {Element} Html element
     */
    render() {
        const { sort, order } = this.state;
        return (
            <div style={{ textAlign: 'center' }}>
                <Button size='mini' name='publishDate' type='sort'
                        active={sort === 'publishDate'} onClick={this.handleChange}>
                    <Icon name='calendar' />
                    Date
                </Button>
                <Button size='mini' name='title' type='sort'
                        active={sort === 'title'} onClick={this.handleChange}>
                    <Icon name='font' />
                    Book title
                </Button>
                <Button size='mini' name='author.name' type='sort'
                        active={sort === 'author.name'} onClick={this.handleChange}>
                    <Icon name='user outline' />
                    Author name
                </Button>
                <Button size='mini' circular icon='sort content ascending' name='asc'
                        active={order === 'asc'} type='order' onClick={this.handleChange} />
                <Button size='mini' circular icon='sort content descending' name='desc'
                        active={order === 'desc'} type='order' onClick={this.handleChange} />
            </div>
        );
    }
}

BooksSort.propTypes = {
    onChange: PropTypes.func
};

BooksSort.defaultProps = {
    onChange: f => f
};


export { BooksSort };
