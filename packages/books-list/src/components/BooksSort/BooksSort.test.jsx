import React from 'react';
import renderer from 'react-test-renderer';
import { BooksSort } from './BooksSort';

describe('BooksSort', () => {
    test('Renders correctly', () => {
        const jsx = (<BooksSort />);
        const tree = renderer
            .create(jsx)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
