import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Loader, Segment } from 'semantic-ui-react';

import { BookListEntry } from '../BookListEntry';
import stylesheet from './booksList.scss';

/**
 * Books list component
 * @constructor
 */
class BooksList extends Component {
    scroller = null;

    /**
     * Initialize scroll reference
     * @param {HTMLElement} ref Reference
     */
    initializeScroll = (ref) => {
        this.scroller = ref;
    };

    /**
     * Handle scroll
     * @param {Event} event Event
     */
    handleScroll = (event) => {
        const { onScroll } = this.props;
        const element = event.srcElement;
        const scrollState = (element.scrollTop / (element.scrollHeight - element.offsetHeight)) * 100;

        onScroll(scrollState);
    };

    /**
     * Component did mount
     */
    componentDidMount() {
        if (this.scroller) {
            this.scroller.addEventListener('scroll', this.handleScroll, true);
        }
    }

    /**
     * Render method
     * @return {Element} Html element
     */
    render() {
        const { books = [], preLoading, postLoading } = this.props;

        return (
            <div className='books-list' ref={this.initializeScroll}>
                <style dangerouslySetInnerHTML={{__html: stylesheet}}/>
                {preLoading && <Segment vertical>
                    <Loader active inline='centered' />
                </Segment>}
                {
                    books.map(book => <BookListEntry book={book} key={book._id} />)
                }
                {postLoading && <Segment vertical>
                    <Loader active inline='centered' />
                </Segment>}
            </div>
        );
    }
}

BooksList.propTypes = {
    books: PropTypes.arrayOf(PropTypes.object),
    preLoading: PropTypes.bool.isRequired,
    postLoading: PropTypes.bool.isRequired,
    fetchBooks: PropTypes.func.isRequired,
    onScroll: PropTypes.func
};

BooksList.defaultTypes = {
    books: [],
    onScroll: f => f
};

export { BooksList };
