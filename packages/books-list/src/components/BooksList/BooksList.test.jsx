import React from 'react';
import renderer from 'react-test-renderer';
import { BooksList } from './BooksList';

describe('BooksList', () => {
    test('Renders correctly', () => {
        const jsx = (<BooksList fetchBooks={f => f} preLoading={false} postLoading={false} />);
        const tree = renderer
            .create(jsx)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('Renders preloader', () => {
        const jsx = (<BooksList fetchBooks={f => f} preLoading={true} postLoading={false} />);
        const tree = renderer
            .create(jsx)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('Renders postloader', () => {
        const jsx = (<BooksList fetchBooks={f => f} preLoading={false} postLoading={true} />);
        const tree = renderer
            .create(jsx)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });
});
