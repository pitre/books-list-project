# Books list

## Prerequisites

- node8
- yarn

## Install

1. Clone this repository.
2. Run `yarn`.

## Running the application

1. yarn `start`
2. Check it out under:  `http://localhost:3000`
