import mongojs from 'mongojs';

/**
 * Mongo Client.
 * Wrapper for mongojs package that uses promises
 */
class MongoClient {
    /**
     * Creates a MongoClient
     *
     * @param {{ host: string, collection: string }} config Configuration for database
     *
     * @public
     */
    constructor(config) {
        this.config = config;
        this.db = null;
        this.collection = null;
        this.connected = false;
    }

    /**
     * Handles the connection of this client to the actual mongo database.
     *
     * @returns {Promise} Promise to handle the module with await
     */
    connect() {
        return new Promise((resolve, reject) => {
            console.log('Connecting to database, please wait...');
            this.db = mongojs(this.config.host, [this.config.collection]);

            this.db.on('connect', () => {
                console.log('Connected!');
                this.collection = this.db.collection(this.config.collection);
                this.connected = true;
                resolve();
            });

            /*
             We need this ping due a bug in the mongojs library, not throwing the connected event till
             at least one operation occurs
             */
            this.db.on('error', (err) => {
                console.log('Error while connection to database', err);
                reject(err);
            });

            /**
             * Seems like the library is buggy and the connect event is not thrown
             * unless an operation is perform
             */
            this.db.runCommand({ ping: 1 });
        });
    }

    /**
     * Inserts an array of documents into the catalog. We use the bulk approach.
     * The batching is handled in another level to guarantee avoid huge requests.
     *
     * @param {Array} documents Array of objects to be inserted
     *
     * @returns {Promise}
     */
    insertDocuments(documents) {
        return new Promise((resolve, reject) => {
            const bulk = this.collection.initializeOrderedBulkOp();

            documents.forEach((doc) => {
                bulk.insert(doc);
            });

            bulk.execute((err) => {
                if (!err) {
                    resolve();
                } else {
                    reject(err);
                }
            });
        });
    }

    /**
     * Performs a request to the mongodb to retrieve the count of documents based on find.
     *
     * @param {{skip: number, limit: number, sort: number, filters: Object}} params Parameters.
     *
     * @returns {Promise.<Array, Error>}
     */
    getDocumentsCount(params) {
        return new Promise((resolve, reject) => {
            this.collection
                .find(params.filters)
                .count((err, docs) => {
                    if (!err) {
                        resolve(docs);
                    } else {
                        reject(err);
                    }
                });
        });
    }

    /**
     * Performs a request to the mongodb to retrieve an Array of Documents.
     *
     * @param {{skip: number, limit: number, sort: Object, filters: Object}} params Parameters.
     *
     * @returns {Promise.<Array, Error>}
     */
    getDocuments(params) {
        console.log('DB Reequest', params);

        return new Promise((resolve, reject) => {
            this.getDocumentsCount(params)
                .then((total) => {
                    this.collection
                        .find(params.filters)
                        .sort(params.sort)
                        .skip(params.skip)
                        .limit(params.limit, (err, docs) => {
                            if (!err) {
                                console.log('Get documents completed!');
                                resolve({
                                    docs,
                                    total
                                });
                            } else {
                                reject(err);
                            }
                        });
                });
        });
    }

    /**
     * Deletes the whole collection.
     * Useful to wipe the database before populating again.
     *
     * @returns {Promise.<Object, Error>}
     */
    removeCollection() {
        return new Promise((resolve, reject) => {
            console.log('Removing entries from database...');
            this.collection.remove((err, res) => {
                if (!err) {
                    console.log('Database entries removed: ', res.n);
                    resolve(res);
                } else {
                    reject(err);
                }
            });
        });
    }

    /**
     * Returns the number of documents inside the database
     *
     * @returns {Promise<number, Error>}
     */
    getNumberOfDocuments() {
        return new Promise((resolve, reject) => {
            this.collection.runCommand('count', (err, res) => {
                if (!err) {
                    resolve(res.n);
                } else {
                    reject(err);
                }
            });
        });
    }

    /**
     * Create index
     * @param {Object} keys Keys object
     * @returns {Promise<any>}
     */
    createIndex(keys) {
        return new Promise((resolve, reject) => {
            this.collection.ensureIndex(keys, {}, (err, res) => {
                if (!err) {
                    resolve(res);
                } else {
                    reject(err);
                }
            });
        });
    }

    /**
     * Closes the connection with the database.
     */
    close() {
        console.log('Connection to DB closed');
        this.db.close();
    }
}

export default MongoClient;
