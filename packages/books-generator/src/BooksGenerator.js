import moment from 'moment';
import Chance from 'chance';
import randomTitle from 'random-title';
import randomDate from 'random-datetime';

import * as constants from './constants';
import Book from './Book';
import Author from './Author';

/**
 * BooksGenerator class
 * @class
 */
class BooksGenerator {

    /**
     * Constructor
     * @constructor
     */
    constructor() {
        this.chance = new Chance();
    }

    /**
     * Get random gender
     * @return {string} Gender
     */
    getRandomGender() {
        return this.chance.pickone(Object.values(constants.GENDERS));
    }

    /**
     * Get random book genre
     * @return {Object} Book genre
     */
    getRandomGenre() {
        return this.chance.pickone(Object.values(constants.GENRES));
    }

    /**
     * Get random date
     * @return {Date} Date
     */
    getRandomDate() {
        return moment.utc(randomDate()).toDate();
    }

    /**
     * Get random author
     * @return {Author} Author
     */
    generateRandomAuthor() {
        const gender = this.getRandomGender();

        return new Author(
            this.chance.name({ gender }),
            gender
        );
    }

    /**
     * Get random book
     * @return {Book} Book
     */
    generateRandomBook() {
        return new Book(
            randomTitle({ min: 1, max: 4 }),
            this.generateRandomAuthor(),
            this.getRandomGenre(),
            this.getRandomDate()
        );
    }

    /**
     * Generate special case books
     * - horror book published on Halloween
     * - finance book published on last Friday
     * @return {Array<Book>}
     */
    generateExtraBooks() {
        return [
            new Book(
                randomTitle({ min: 1, max: 4 }),
                this.generateRandomAuthor(),
                constants.GENRES.horror,
                moment.utc([2222, 9, 31]).toDate(),
            ),
            new Book(
                randomTitle({ min: 1, max: 4 }),
                this.generateRandomAuthor(),
                constants.GENRES.finance,
                moment.utc([3333, 2, 27]).toDate(),
            )
        ];
    }

    /**
     * Generate count number of random books
     * @param {number} count Number of books to generate
     * @return {Array<Book>} Books
     */
    generateBooks(count) {
        const books = [];

        for (let i = 0; i < count; i += 1) {
            books.push(this.generateRandomBook());
        }

        return books;
    }
}

export default BooksGenerator;
