export const DEFAULT_AUTHOR_NAME = 'Gal Anonim';

export const DEFAULT_BOOK_TITLE = 'Interesting Book';
export const DEFAULT_BOOK_GENRE = 'Unknown';

export const GENDERS = Object.freeze({
    FEMALE: 'female',
    MALE: 'male'
});
export { GENRES } from '@pw/books-list';

