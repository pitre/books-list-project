const config = {
    host: 'mongodb://localhost:27017/books',
    collection: 'books',
    batchSize: 50000,
    booksCount: 1000000
};

export default config;
