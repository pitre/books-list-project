import { MongoClient } from '@pw/mongo-client';
import BooksGenerator from './BooksGenerator';
import config from './config';

const booksGenerator = new BooksGenerator();

/**
 * Insert extra books for special cases
 * @param {Object} mongoClient Mongo client
 * @return {Promise<number>} Promise
 */
const insertExtraBooks = async (mongoClient) => {
    const books = booksGenerator.generateExtraBooks();

    await mongoClient.insertDocuments(books);
    return books.length;
};

/**
 * Insert count books in batchSize
 * @param {Object} mongoClient Mongo client
 * @param {number} count Number of books
 * @param (number} batchSize Batch size
 * @return {Promise<void>} Promise
 */
const insertBooks = async (mongoClient, count, batchSize) => {
    for (let i = 0; i < count; i += batchSize) {
        const size = Math.min(batchSize, count - i);

        await mongoClient.insertDocuments(booksGenerator.generateBooks(size));
    }
};

/**
 * Parse process args
 * @return {{booksCount: number, batchSize: number}} Process args
 */
const parseArgs = () => {
    const booksCount = parseInt(process.argv[2] || config.booksCount, 10);
    const batchSize = parseInt(process.argv[3] || config.batchSize, 10);
    const host = process.argv[4] || config.host;
    const collection = process.argv[5] || config.collection;

    return {
        booksCount,
        batchSize,
        host,
        collection
    };
};

(async () => {
    const { booksCount, batchSize, host, collection } = parseArgs();
    const mongoClient = new MongoClient({ host, collection });

    try {
        if (booksCount === 1000000) {
            console.log('This will take around 5min. Go and get yourself a coffee :D');
        }
        await mongoClient.connect();
        await mongoClient.removeCollection();

        await mongoClient.createIndex({ title: 1 });
        await mongoClient.createIndex({ title: -1 });
        await mongoClient.createIndex({ publishDate: 1 });
        await mongoClient.createIndex({ publishDate: -1 });
        await mongoClient.createIndex({ 'author.name': 1 });
        await mongoClient.createIndex({ 'author.name': -1 });
        await mongoClient.createIndex({ 'author.gender': 1 });
        await mongoClient.createIndex({ 'author.gender': -1 });

        const extra = await insertExtraBooks(mongoClient);

        await insertBooks(mongoClient, booksCount - extra, batchSize);

        console.log(`We have ${await mongoClient.getNumberOfDocuments()} books in db`);
    } catch (e) {
        console.error(e);
    } finally {
        await mongoClient.close();
    }
})();

