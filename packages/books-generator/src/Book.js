import moment from 'moment';
import * as constants from './constants';
import Author from './Author';

/**
 * Author class
 */
class Book {
    /**
     * Constructor
     * @param {string} title Books title
	 * @param {Author} author Books author
	 * @param {Object} genre Books genre
     * @param {Date} publishDate Publish date
     * @constructor
     */
    constructor(title = constants.DEFAULT_BOOK_TITLE,
        author = new Author(),
        genre = constants.DEFAULT_BOOK_GENRE,
        publishDate = moment.utc([1970, 0, 1]).toDate()) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.publishDate = publishDate;
    }

    /**
     * Serialize author to json
     * @return {{name: Author.name, gender: Author.gender}} Serialized author object
     */
    toJSON() {
        const { title, author, genre, publishDate } = this;

        return {
            title,
            author,
            genre,
            publishDate
        };
    }
}

export default Book;
