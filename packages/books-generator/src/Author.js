import * as constants from './constants';

/**
 * Author class
 */
class Author {
    /**
	 * Constructor
     * @param {string} name Authors name
     * @param {string} gender Authors gender
	 * @constructor
     */
    constructor(name = constants.DEFAULT_AUTHOR_NAME, gender = constants.GENDERS.MALE) {
        this.name = name;
        this.gender = gender;
    }

    /**
	 * Serialize author to json
     * @return {{name: Author.name, gender: Author.gender}} Serialized author object
     */
    toJSON() {
        const { name, gender } = this;

        return { name, gender };
    }
}

export default Author;
