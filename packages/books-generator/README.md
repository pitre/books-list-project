# Random books generator

Generates JSON object with random book data.

* It takes around 300s to populate 1M database. 

## Installation
```bash
yarn
```

## Populate database

```
yarn generate [books count] [batch size] [host] [collection]
```
